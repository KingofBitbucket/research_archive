import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

'''
objects = ('Machine#1', 'Machine#2', 'Machine#3', 'Machine#4')
y_pos = np.arange(len(objects))
performance = [4.89, 4.53, 1.02, 0.61]
 
plt.bar(y_pos, performance, align='center', color='g', width=0.5)
plt.xticks(y_pos, objects)
plt.ylabel('Memory Usage (GB)')
plt.title('SegNet Memory Allocation (m-SCT)')

plt.savefig('results/segnet_memory')

plt.show()
'''

objects = ('Device#1', 'Device#2', 'Device#3', 'Device#4')
y_pos = np.arange(len(objects))
mem = [4.89, 4.53, 1.02, 0.61] #SegNet
cpu = [29.40, 19.30, 21.10, 4.20]
#mem = [8.20, 6.57, 4.47, 3.52] #Inception
#cpu = [18.49, 18.44, 19.63, 12.71]
#mem = [80.56, 57.68, 48.93, 28.95] #RNNLM
#cpu = [99.0, 35.2, 108.4, 35.7]

fig, ax1 = plt.subplots()
plt.xticks(y_pos, objects)
plt.title('SegNet Memory and CPU Usage (m-SCT)')
green_patch = mpatches.Patch(facecolor='g', label='Memory', hatch='\\\\\\')
blue_patch = mpatches.Patch(facecolor='b', label='CPU', hatch='///')
plt.legend(handles=[green_patch, blue_patch])

ax1.set_ylabel('Memory Allocation (GB)', color='g')
ax1.bar(y_pos - 0.175, mem, label='Memory', color='g', width=0.35, hatch='\\')
ax1.tick_params(axis='y', labelcolor='g')

ax2 = ax1.twinx()
ax2.set_ylabel('Computation Time Per Iteration (s)', color='b')
ax2.bar(y_pos + 0.175, cpu, label='CPU', color='b', width=0.35, hatch='/')
ax2.tick_params(axis='y', labelcolor='b')

fig.tight_layout()
plt.savefig('results/segnet_combined.png')
plt.show()