# File Hierarachy
This repository is only for archiving important files for the research of memory constraint device placement. Most of the implementation files lie in *Implementation*. The simulate.py contains the code for both simulation and device placement on TensorFlow. Notice that we need to modify the training code to support the distributed training and call the simulator, so the code is subject to the specific model. If readers want a running example, they can refer to another repository called *Submission*.

The *Plots* contains both the code for generating plots and the plots themselves.

deploy_raw_data.docx and simulation_raw_data.docx are the records of experiments. mosek.lic is for the use of sct.

    Implementation
        simulate.py
        m_sct.py
        m_etf.py
        m_topo.py
    Plots
        cnn_simulation_results
        inception_simulation_results
        deploy_results
        deploy_plot.py
        simulation_plot.py
    deploy_raw_data.docx
    simulation_raw_data.docx
    mosek.lic

